package com.rezilux.test.Controller;

@Controller
public class mail {

    @Autowired
    public JavaMailSender emailSender;

    @ResponseBody
    @RequestMapping("/envoimail")
    public String sendSimpleEmail() {

        // Create a Simple MailMessage.
        SimpleMailMessage message = new SimpleMailMessage();

        message.setTo(MyConstants.FRIEND_EMAIL);
        message.setSubject("Test Simple Email");
        message.setText("Hello, Im testing Simple Email");

        // Send Message!
        this.emailSender.send(message);

        return "Email Sent!";
    }

}