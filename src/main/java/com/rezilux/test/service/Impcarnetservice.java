package com.rezilux.test.service;

import com.rezilux.test.dao.CarnetRepository;
import com.rezilux.test.dao.ContratRepository;
import com.rezilux.test.entites.Carnet;
import com.rezilux.test.entites.Contrat;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.stream.Stream;
@Service
@Transactional
public class Impcarnetservice implements carnetservice{

    @Autowired
    private CarnetRepository carnetRepository;
    @Autowired
    private ContratRepository contratRepository;

    @Override
    public void initcarnet() {
        // TODO Auto-generated method stub
        Stream.of("C1", "C2", "C3").forEach(nomc -> {
            Carnet c= new Carnet();
            c.setNomCarnet(nomc);
            carnetRepository.save(c);
        });
    }

    @Override
    public void initcontrat() {

        carnetRepository.findAll().forEach(nomc -> {
            Stream.of("contrat1", "contrat2","contrat3","contrat4","contrat5","contrat6","contrat7").forEach(cont -> {
                Contrat contrat = new Contrat();
                contrat.setVille("DAKAR");
                contrat.setAdresse("Yoff virage Lot 24 , Dakar - Sénégal");
                contrat.setEmail("contrat@gmaim.com");
                contrat.setNomcomplet(cont);
                contrat.setPays("Senegal");
                contrat.setTelephone("78 900 34 55");
                contrat.setCarnet(nomc);
                contratRepository.save(contrat);
            });
        });

    }
}
