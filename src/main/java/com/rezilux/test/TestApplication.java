package com.rezilux.test;

import com.rezilux.test.dao.CarnetRepository;
import com.rezilux.test.entites.Carnet;
import com.rezilux.test.service.Impcarnetservice;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.data.rest.core.config.RepositoryRestConfiguration;

@SpringBootApplication
public class TestApplication implements  CommandLineRunner{

	@Autowired
	private Impcarnetservice impcarnetservice;

	public static void main(String[] args) {
		SpringApplication.run(TestApplication.class, args);
	}


	@Override
	public void run(String... args) throws Exception {
		impcarnetservice.initcarnet();
		impcarnetservice.initcontrat();
	}
}
