package com.rezilux.test.entites;

import com.sun.istack.NotNull;
import lombok.*;

import javax.annotation.processing.Generated;
import javax.persistence.*;
import java.io.Serializable;
import java.util.Collection;

@Entity
public class Carnet implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    @OneToMany(mappedBy = "carnet")
    private Collection<Contrat> contrats;
    private String nomCarnet;

    public Carnet() {
    }

    public Carnet(Collection<Contrat> contrats, String nomCarnet) {
        this.contrats = contrats;
        this.nomCarnet = nomCarnet;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Collection<Contrat> getContrats() {
        return contrats;
    }

    public void setContrats(Collection<Contrat> contrats) {
        this.contrats = contrats;
    }

    public String getNomCarnet() {
        return nomCarnet;
    }

    public void setNomCarnet(String nomCarnet) {
        this.nomCarnet = nomCarnet;
    }

    @Override
    public String toString() {
        return "Carnet{" +
                "id=" + id +
                ", contrats=" + contrats +
                ", nomCarnet='" + nomCarnet + '\'' +
                '}';
    }
}

