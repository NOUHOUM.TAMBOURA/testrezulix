package com.rezilux.test.dao;

import com.rezilux.test.entites.Carnet;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.web.bind.annotation.CrossOrigin;

@CrossOrigin("*")
@RepositoryRestResource
public interface CarnetRepository extends JpaRepository<Carnet, Long> {

}
