package com.rezilux.test.dao;

import com.rezilux.test.entites.Carnet;
import com.rezilux.test.entites.Contrat;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.web.bind.annotation.CrossOrigin;

@CrossOrigin("*")
@RepositoryRestResource
public interface ContratRepository  extends JpaRepository<Contrat, Long> {
}
